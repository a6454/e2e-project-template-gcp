import e2e_project_template_gcp as eptg
import e2e_project_template_gcp_fastapi as eptg_fapi


PRED_MODEL = eptg.modeling.utils.load_model(
    eptg_fapi.config.SETTINGS.PRED_MODEL_PATH)
