import logging
import fastapi
from fastapi.middleware.cors import CORSMiddleware

import e2e_project_template_gcp as eptg
import e2e_project_template_gcp_fastapi as eptg_fapi


LOGGER = logging.getLogger(__name__)
LOGGER.info("Setting up logging configuration.")
eptg.general_utils.setup_logging(
    logging_config_path=eptg_fapi.config.SETTINGS.LOGGER_CONFIG_PATH)

APP = fastapi.FastAPI(
    title=eptg_fapi.config.SETTINGS.API_NAME,
    openapi_url=f"{eptg_fapi.config.SETTINGS.API_V1_STR}/openapi.json")
API_ROUTER = fastapi.APIRouter()
API_ROUTER.include_router(
    eptg_fapi.v1.routers.model.ROUTER, prefix="/model", tags=["model"])
APP.include_router(
    API_ROUTER, prefix=eptg_fapi.config.SETTINGS.API_V1_STR)

ORIGINS = ["*"]

APP.add_middleware(
    CORSMiddleware,
    allow_origins=ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"])
